#pragma once

#include <string>
#include <random>

class PasswordGenerator
{
	std::string const charset = "1234567890ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba";

public:
	std::string generate(size_t length = 50)
	{
		std::random_device rand;
		std::string pass;
		pass.reserve(length);
		for (size_t i = 0; i < length; ++i)
		{
			size_t index = static_cast<size_t>(rand() % charset.size());
			pass.push_back(charset[index]);
		}
		return pass;
	}

};

