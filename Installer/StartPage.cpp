#include "StartPage.h"

#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>

StartPage::StartPage(QWizard* parent)
	: QWizardPage{ parent }
{
	QVBoxLayout* layout = new QVBoxLayout;
	setLayout(layout);

	QLabel* label = new QLabel{ "This program will", this };
	layout->addWidget(label);

	{
		QHBoxLayout* pathLayout = new QHBoxLayout;
		QLabel* label = new QLabel{ "Select install directory:", this };
		pathLayout->addWidget(label);

		_openDialogButton = new QPushButton{ "...", this };
		QObject::connect(_openDialogButton, &QPushButton::clicked, this, &StartPage::onOpenDialog);

		_pathLabel = new QLabel{ this };

		pathLayout->addWidget(_openDialogButton);
		pathLayout->addWidget(_pathLabel);

		layout->addLayout(pathLayout);
	}
}

bool StartPage::isComplete() const
{
	return !path().isEmpty();
}

QString StartPage::path() const
{
	return _pathLabel->text();
}

void StartPage::onOpenDialog()
{
	QString path = QFileDialog::getExistingDirectory(this, "", "", QFileDialog::Option::ShowDirsOnly);
	if (path.isEmpty())
		return;

	_pathLabel->setText(path);
	emit completeChanged();
}