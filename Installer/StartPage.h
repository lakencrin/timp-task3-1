#pragma once
#include <qwizard.h>
class StartPage : public QWizardPage
{
	Q_OBJECT

	class QPushButton* _openDialogButton;
	class QLabel* _pathLabel;

public:
	StartPage(QWizard* parent);

	QString path() const;

	bool isComplete() const override;

private slots:
	void onOpenDialog();
};

