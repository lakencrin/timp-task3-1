#include "InstallPage.h"
#include "StartPage.h"
#include "Utils.h"
#include "PasswordGenerator.h"
#include "AES.h"

#include <QFile>
#include <QTimer>
#include <QDir>
#include <QJsonDocument>
#include <QVBoxLayout>
#include <QProgressBar>
#include <QLabel>

InstallPage::InstallPage(QWizard* parent)
	: QWizardPage{ parent }
	, _timer{ new QTimer }
{
	QVBoxLayout* layout = new QVBoxLayout;
	setLayout(layout);

	QLabel* label = new QLabel{ "Wait until the program will install the update" };
	layout->addWidget(label);

	_progressBar = new QProgressBar;
	_progressBar->setRange(0, 100);
	_progressBar->setValue(0);
	layout->addWidget(_progressBar);

	QObject::connect(_timer, &QTimer::timeout, this, &InstallPage::onTimeout);
	_timer->setInterval(50);
}

void InstallPage::initializePage()
{
	QString const path = dynamic_cast<StartPage*>(wizard()->page(0))->path();
	QDir pathDir{ path };
	QString const sysTatFilePath = pathDir.absoluteFilePath("sys.tat");

	QString const secureExeFilePath = pathDir.absoluteFilePath("secure.exe");
	QFile secureExeResourceFile{ ":/Resource/Secure.exe" };
	secureExeResourceFile.open(QFile::ReadOnly);
	
	QFile secureExeFile{ secureExeFilePath };
	secureExeFile.open(QFile::WriteOnly);
	secureExeFile.write(secureExeResourceFile.readAll());
	Utils::registerDefaultProgram(secureExeFilePath);

	QByteArray contents = QJsonDocument{ Utils::getSystemInfo().toJson() }.toJson();

	_password = QString::fromStdString(PasswordGenerator{}.generate(16));

	QFile sysTatFile{ sysTatFilePath };
	sysTatFile.open(QFile::OpenModeFlag::WriteOnly);
	sysTatFile.write(AES{ 128 }.encrypt(contents, _password));

	_timer->start();
}

void InstallPage::onTimeout()
{
	_progressBar->setValue(_progressBar->value() + 1);

	if (isComplete())
	{
		QLabel* passwordLabel = new QLabel;
		passwordLabel->setText(QString{ "Write down this secret password: <strong>%1<strong>" }.arg(_password));
		layout()->addWidget(passwordLabel);

		Utils::writeSecretPassword(_password.toStdString());

		_timer->stop();
		emit completeChanged();
	}
}

bool InstallPage::isComplete() const
{
	return _progressBar->value() == 100;
}