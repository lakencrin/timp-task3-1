#pragma once
#include <QWizardPage>

class InstallPage : public QWizardPage
{
	class QProgressBar* _progressBar;
	class QTimer* _timer;

	QString _password;

public:
	InstallPage(QWizard* parent);

	void initializePage() override;

	bool isComplete() const override;

private slots:
	void onTimeout();
};

