#pragma once

#include "SystemInfo.h"

#include <QString>

#include <string>

class Utils
{
public:
	static ComputerInfo getComputerInfo();
	static QString getUserName();
	static quint64 getRamKb();
	static QString getProcessorName();
	static QString getOsName();

	static SystemInfo getSystemInfo();

	static void writeSecretPassword(const std::string& password);

	static void registerDefaultProgram(const QString& path);

	static bool isElevated();
};