#include "Wizard.h"

#include "StartPage.h"
#include "InstallPage.h"

Wizard::Wizard()
{
	addPage(new StartPage{ this });
	addPage(new InstallPage{ this });
}