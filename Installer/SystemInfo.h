#pragma once

#include <QString>
#include <QJsonObject>

struct ComputerInfo
{
	QString computerName;
	QString domainName;
};

struct SystemInfo
{
	QString userName;
	ComputerInfo computerInfo;

	quint64 ram;
	QString processorName;

	QString osName;

	QJsonObject toJson() const;
	void fromJson(const QJsonObject&);
};