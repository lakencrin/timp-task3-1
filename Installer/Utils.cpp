#include "Utils.h"
#include "Keccak.h"

#include <Windows.h>
#include <LM.h>
#include <VersionHelpers.h>

SystemInfo Utils::getSystemInfo()
{
	return {
		Utils::getUserName(),
		Utils::getComputerInfo(),
		Utils::getRamKb(),
		Utils::getProcessorName(),
		Utils::getOsName()
	};
}


QString Utils::getUserName() 
{
	wchar_t buffer[1000];
	DWORD bufferSize = 1000;
	GetUserNameW(buffer, &bufferSize);
	return QString::fromStdWString(buffer);
}

ComputerInfo Utils::getComputerInfo()
{
	LPWKSTA_INFO_102 info;
	DWORD result = NetWkstaGetInfo(nullptr, 102, (LPBYTE*)&info);
	return { QString::fromStdWString(info->wki102_computername), QString::fromStdWString(info->wki102_langroup) };
}

quint64 Utils::getRamKb()
{
	quint64 ram;
	GetPhysicallyInstalledSystemMemory(&ram);
	return ram;
}

QString Utils::getProcessorName()
{
	LSTATUS result;

	HKEY handle;
	result = RegOpenKeyW(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", &handle);
	if (result != ERROR_SUCCESS)
		return QString{};

	wchar_t buffer[1000];
	DWORD size = 1000;
	result = RegGetValueW(handle, nullptr, L"ProcessorNameString", RRF_RT_REG_SZ, nullptr, buffer, &size);
	if (result != ERROR_SUCCESS)
		return QString{};

	return QString::fromStdWString(buffer);
}

QString Utils::getOsName()
{
	DWORD dwVersion = GetVersion();

	DWORD dwMajorVersion = (DWORD)(LOBYTE(LOWORD(dwVersion)));
	DWORD dwMinorVersion = (DWORD)(HIBYTE(LOWORD(dwVersion)));
	DWORD dwBuild = 0;
	if (dwVersion < 0x80000000)
		dwBuild = (DWORD)(HIWORD(dwVersion));

	return (dwBuild != 0 ? QString{ "Windows %1.%2 (build %3)" } : QString{ "Windows %1.%2" }).arg(dwMajorVersion).arg(dwMinorVersion).arg(dwBuild);
}

void Utils::writeSecretPassword(const std::string& passwordStd)
{
	LSTATUS status;
	std::string const passwordHash = Keccak{}(passwordStd.c_str(), passwordStd.size());

	HKEY key;
	status = RegCreateKeyW(HKEY_CURRENT_USER, L"SOFTWARE\\SHABAEV", &key);
	status = RegSetValueExA(key, "Signature", 0, RRF_RT_REG_SZ, (const BYTE*)passwordHash.c_str(), passwordHash.length() + 1);
}

bool Utils::isElevated() 
{
	BOOL fRet = FALSE;
	HANDLE hToken = NULL;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken)) 
	{
		TOKEN_ELEVATION Elevation;
		DWORD cbSize = sizeof(TOKEN_ELEVATION);

		if (GetTokenInformation(hToken, TokenElevation, &Elevation, sizeof(Elevation), &cbSize))
			fRet = Elevation.TokenIsElevated;
	}

	if (hToken)
		CloseHandle(hToken);

	return fRet;
}

void Utils::registerDefaultProgram(const QString& path)
{
	LSTATUS status;

	HKEY key;
	status = RegDeleteTreeW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Classes\\.tat");
	status = RegCreateKeyW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Classes\\.tat\\shell\\open\\command", &key);

	std::string const valueData = QString{ path }.replace("/", "\\").toStdString() + " \"%1\"";
	status = RegSetValueExA(key, nullptr, 0, REG_EXPAND_SZ, (const BYTE*)valueData.c_str(), valueData.length() + 1);
}