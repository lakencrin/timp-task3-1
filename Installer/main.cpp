#include "Utils.h"
#include "Wizard.h"
#include "AES.h"

#include <QApplication>
#include <QFileDialog>
#include <QJsonDocument>
#include <QMessageBox>
#include <QFile>

#include <fstream>

int main(int argc, char* argv[])
{
	QApplication app{ argc, argv };
	if (!Utils::isElevated())
	{
		QMessageBox::critical(nullptr, "Error", "Run program as administrator to continue");
		return 1;
	}

	Wizard wizard;
	wizard.show();

	return app.exec();
}