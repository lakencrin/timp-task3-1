#include "SystemInfo.h"

QJsonObject SystemInfo::toJson() const
{
	QJsonObject object;
	object.insert("userName", userName);

	QJsonObject computerInfo;
	computerInfo.insert("computerName", this->computerInfo.computerName);
	computerInfo.insert("domainName", this->computerInfo.domainName);
	object.insert("computerInfo", computerInfo);

	object.insert("ram", QString::number(ram));
	object.insert("processorName", processorName);
	object.insert("osName", osName);
	return object;
}

void SystemInfo::fromJson(const QJsonObject& object)
{
	userName = object.value("userName").toString();

	QJsonObject computerInfoObject = object.value("computerInfo").toObject();
	computerInfo.computerName = computerInfoObject.value("computerName").toString();
	computerInfo.domainName = computerInfoObject.value("domainName").toString();

	ram = object.value("ram").toInt();
	processorName = object.value("processorName").toInt();
	osName = object.value("osName").toInt();
}