#include "Utils.h"

#include <Windows.h>

QString Utils::securableFilePath()
{
	wchar_t buffer[1000];
	DWORD size = 1000;

	GetModuleFileNameW(NULL, buffer, 1000);
	QString path = QString::fromStdWString(buffer);

	path = path.mid(0, path.lastIndexOf(L'\\') + 1) + "sys.tat";
	return path;
}

std::string Utils::hashedKey()
{
	LSTATUS status;

	HKEY key;
	status = RegOpenKeyW(HKEY_CURRENT_USER, L"SOFTWARE\\SHABAEV", &key);

	char buffer[1000]{};
	DWORD size = 1000;
	status = RegGetValueA(key, nullptr, "Signature", RRF_RT_REG_SZ, nullptr, buffer, &size);
	return buffer;
}