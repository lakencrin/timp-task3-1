#pragma once

#include <QString>

class Utils
{

public:
	static QString securableFilePath();
	static std::string hashedKey();
};

