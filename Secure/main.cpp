#include "Utils.h"
#include "AES.h"
#include "Keccak.h"

#include <QFileInfo>
#include <QDebug>
#include <QTextStream>

#include <iostream>

int main(int argc, char* argv[])
try
{
	QString path;
	if (argc < 2)
	{
		path = Utils::securableFilePath();
		QFileInfo securableFileInfo{ path };
		if (!securableFileInfo.exists())
			throw std::exception{ "Can\'t locate sys.tat file" };

		qInfo() << "Located sys.tat file near the secur.exe: " << path;
	}
	else
	{
		path = argv[1];
		QFileInfo securableFileInfo{ path };
		if (!securableFileInfo.exists())
			throw std::exception{ "Path to \'tat\' file is wrong. No such file was found." };

		qInfo() << QString{ "Located %1." }.arg(securableFileInfo.fileName());
	}

	QFile securableFile{ path };
	securableFile.open(QFile::ReadOnly);

	std::cout << "Enter the password: ";
	std::string secretPasswordStd;
	std::cin >> secretPasswordStd;
	QString const secretPassword = QString::fromStdString(secretPasswordStd);
	if (Keccak{}(secretPassword.toStdString().c_str(), secretPassword.length()) != Utils::hashedKey())
	{
		qCritical() << "Wrong password! Content is not accessible";
		system("pause");
		return 1;
	}

	QByteArray decryptedText = AES{ 128 }.decrypt(securableFile.readAll(), secretPassword);
	std::cout << QString{ decryptedText }.toStdString();

	system("pause");

	return 0;
}
catch (std::exception& e)
{
	qInfo() << e.what();
	return -1;
}